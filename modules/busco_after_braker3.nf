process busco_after_braker3 {
	label 'midmem'
	
	tag "busco_after_braker3"

	publishDir "${params.resultdir}/05b_busco",	mode: 'copy', pattern : '*.busco'
	publishDir "${params.resultdir}/logs/busco_after_braker3",	mode: 'copy', pattern : 'busco*.log'
	publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: 'busco*.cmd'

	input:
		path(busco_db_path)
		val(busco_db_name)
		path(prot_fa)
	
	output:
		path("*.busco")
		path("busco*.log")
		path("busco*.cmd")

	script:
	"""
	busco_after_braker3.sh ${task.cpus} ${busco_db_path} ${busco_db_name} ${prot_fa} busco_after_braker3.cmd >& busco_after_braker3.log 2>&1
	"""

}

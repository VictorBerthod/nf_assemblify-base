process braker3 {

    label 'midmem'

    tag "braker3"

    publishDir "${params.resultdir}/05a_braker3",	mode: 'copy', pattern: 'braker3/*.gff3'
    publishDir "${params.resultdir}/05a_braker3",	mode: 'copy', pattern: 'braker3/*.aa'
    publishDir "${params.resultdir}/logs/braker3",	mode: 'copy', pattern: '*.log'
    publishDir "${params.resultdir}/logs/braker3",	mode: 'copy', pattern: 'braker3/*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: '*.cmd'

    input:
        path(red_ch)
        path(bam_ch)
        val(species_ch)
        path(prot_ch)

    output:
        path("braker3/*.gff3"), emit: gff3_ch
        path("braker3/*.aa"), emit: faa_ch
        path("*.log")
        path("braker3/*.log")
        path("*.cmd")
        
    script:
    """
    braker3.sh $red_ch $bam_ch $species_ch $prot_ch ${task.cpus} braker3.cmd >& braker3.log 2>&1
    """ 
}




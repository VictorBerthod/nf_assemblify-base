process samtools {

    label 'midmem'

    tag "samtools_${sam_ch.baseName}"

    publishDir "${params.resultdir}/04b_samtools",	mode: 'copy', pattern: '*.bam'
    publishDir "${params.resultdir}/04b_samtools",    mode: 'copy', pattern: '*.bai'
    publishDir "${params.resultdir}/logs/samtools",	mode: 'copy', pattern: '*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: '*.cmd'

    input:
        each(sam_ch)

    output:
        path("*.bam"), emit: bam_ch
        path("*.bai")
        path("*.log")
        path("*.cmd")
        
    script:
    """
    samtools.sh $sam_ch ${task.cpus} samtools_${sam_ch.baseName}.cmd >& samtools_${sam_ch.baseName}.log 2>&1
    """ 
}




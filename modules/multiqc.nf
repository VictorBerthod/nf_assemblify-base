process multiqc {

    label 'lowmem'

    tag "multiQC"

    publishDir "${params.resultdir}/01b_multiqc",	mode: 'copy', pattern: '*.html'
    publishDir "${params.resultdir}/logs/multiqc",	mode: 'copy', pattern: 'multiqc*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'multiqc*.cmd'

    input:
        path(qc_ch)

    output:
        path("*.html")
        path("multiqc*.log")
        path("multiqc*.cmd")

    script:
    """
    multiqc.sh "." "multiqc_report" multiqc.cmd >& multiqc.log 2>&1
    """ 
}




#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##               	Braker3 annotation (prediction)                          ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
MSK_FASTA=${args[0]} #Output from RED
RNASEQ_BAM=${args[1]} #BAM from converted from Hisat2 output with Samtools
SPECIES=${args[2]} #Species name
PROT=${args[3]} #Protein from short evolutionary distance
NCPUS=${args[4]}
LOGCMD=${args[5]}

# Command to execute
CMD="braker.pl --species=${SPECIES} --genome=${MSK_FASTA} --bam=${RNASEQ_BAM} --prot_seq=${PROT} \
          --workingdir=braker3 --gff3 --threads ${NCPUS}"
       
# Keep command in log
echo $CMD > $LOGCMD

# Run Commands
eval $CMD


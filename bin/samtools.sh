#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                  Samtools for converting SAM to BAM and indexing          ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
SAM_IN=${args[0]}
NCPUS=${args[1]}
LOGCMD=${args[2]}

BAM_OUT=$(basename ${SAM_IN%.*}).bam

CMD1="samtools sort -o $BAM_OUT -@ $NCPUS -m 2G -O bam -T tmp $SAM_IN"
CMD2="samtools index $BAM_OUT"

# Keep command in log
echo $CMD1 > $LOGCMD
echo $CMD2 >> $LOGCMD

# Run Command
eval ${CMD1}
eval ${CMD2}


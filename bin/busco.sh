#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##           Quantitative assessment of genome assembly with BUSCO           ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
NCPUS=${args[0]}
BUSCO_DB_PATH=${args[1]}
BUSCO_DB_NAME=${args[2]}
ASSEMBLY_FILE=${args[3]}
LOGCMD=${args[4]}

# Temporary variable
RES_FILE=$(basename ${ASSEMBLY_FILE%.*}).busco

# Command to execute
CMD="busco -c $NCPUS -m genome --offline -i $ASSEMBLY_FILE -l ${BUSCO_DB_PATH}/${BUSCO_DB_NAME} -o $RES_FILE"

# Keep command in log
echo $CMD > $LOGCMD

# Execute command
eval ${CMD}

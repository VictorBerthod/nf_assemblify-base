#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##           Quantitative assessment of predicted proteins with BUSCO        ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
NCPUS=${args[0]}
BUSCO_DB_PATH=${args[1]}
BUSCO_DB_NAME=${args[2]}
PROT_FILE=${args[3]} #Output from BRAKER3 (.faa)
LOGCMD=${args[4]}

# Temporary variable
RES_FILE=$(basename ${PROT_FILE%.*}).busco

# Command to execute
CMD="busco -c $NCPUS -m proteins --offline -i $PROT_FILE -l ${BUSCO_DB_PATH}/${BUSCO_DB_NAME} -o $RES_FILE"

# Keep command in log
echo $CMD > $LOGCMD

# Execute command
eval ${CMD}

#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##              Plotting tool for long read using fastqc                     ##
##                                                                           ##
###############################################################################

# Get script arguments coming from modules/nanoplot.nf process
args=("$@")
cpus=${args[0]}
FASTQ_FILE=${args[1]}
LOGCMD=${args[2]}


# Command to execute
CMD="NanoPlot -t ${cpus} --plots kde dot hex --N50 --title \"PacBio Hifi reads for $(basename ${FASTQ_FILE%.hifi*})\" --fastq ${FASTQ_FILE} -o ."

# Save command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}


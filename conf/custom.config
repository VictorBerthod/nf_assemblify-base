/*
 * -------------------------------------------------
 *  Nextflow config file for processes options
 * -------------------------------------------------
 * Defines general paths for input files and
 * parameters for your own analysis
 */

params {

    /*
    General parameters
    */
    // Analyzed project name
    projectName = "assemblify"

    // Output directory to publish workflow results
    outdir = "${baseDir}/results/${projectName}"
    resultdir = "${outdir}/02_results"

    // Illumina and Pacbio reads
    datadir = "$ASSEMBLIFY_DATA_PATH"
    illumina_path = "${datadir}/*subset.fastq.gz"
    illumina_r1_path = "${datadir}/*_1.subset.fastq.gz"
    illumina_r2_path = "${datadir}/*_2.subset.fastq.gz"
    pacbio_path = "${datadir}/*20X.fastq.gz"
    
    // Busco constants
    busco_db_path = "$BUSCO_DB_PATH"
    busco_db_name = "chlorophyta_odb10"
    
    //Braker3 constants
    braker3_species_name = "Dunaliella_primolecta"
    braker3_prot_path = "${datadir}/chlamydomonadales.subset.faa"
}

[![Nextflow](https://img.shields.io/badge/nextflow-%E2%89%A520.04.1-23aa62.svg?labelColor=000000)](https://www.nextflow.io/)
[![Containerisation with singularity](https://img.shields.io/badge/run%20with-singularity-1d355c.svg?labelColor=000000)](https://sylabs.io/docs/)

# Introduction
This pipeline was created as part of an exercise in the FAIR courses given as part of the Master 2 Bioinformatics course at Rennes.
The aim is to apply **FAIR principles** to the creation of a pipeline for genome assembly and annotation. It has been designed to run on IFB clusters. 

> You can find the ifremer practical case [here](https://practical-case-bioinfo-teaching-fair-gaa-92d332f8346e321adf6fde.gitlab-pages.ifremer.fr).


# Compute requirements

Minimum requirements:  
- CPUs = 14
- Memory = 24 GB  

<u>Approximate run time:</u>  
3 hours for quick start launch using the minimum requirements.

> (If you would like more information on the specifications of the clusters, please consult the [IFB Core Cluster Documentation](https://ifb-elixirfr.gitlab.io/cluster/doc/cluster-desc/).)

# Quick start
## Source of data 
The quick start uses sequencing data from the *Dunaliella primolecta* genome. The PacBio and Illumina data (RNA-seq) were reduced using 3 scaffolds from the genome with the aim of reducing data volumetry and calculation time for each stage of the pipeline to be developed.
To find all the metadata linked to the data used, go [here](https://practical-case-bioinfo-teaching-fair-gaa-92d332f8346e321adf6fde.gitlab-pages.ifremer.fr/#data).

Reduced data are available in the shared workspace on the cluster:
`/shared/projects/2401_m2_bim/data/assemblify`



## How to launch ?

You must first login to [IFB Institut Français de Bioinformatique](https://login.cluster.france-bioinformatique.fr/ "IFB login page").  

Once you have connected to the server, you can move to the following directory: `/shared/projects/2401_m2_bim/app-2401-14/nf_assemblify-base` with the command : 
```bash
git clone https://gitlab.com/VictorBerthod/nf_assemblify-base.git
cd nf_assemblify-base
```

You can launch the pipeline directly by running the `run_assemblify.slurm` file on one of the computing server using the following command:
```bash
#Run the whole pipeline
sbatch run_assemblify.slurm
```
**This pipeline can also be launched on any computing server that uses SLURM, Nextflow and Singularity and is not exclusive to IFB servers.**
# Customize the pipeline
If you don't have access to IFB servers or if you want to change the species studied, you can customize this pipeline by making a few modifications and following the installation instructions below.

## Data directory

Once the pipeline has been installed, you need to specify the path to the directory containing your data to be processed. To do this, go to the `run_assemblify.slurm` file. You need to change the path `/YOUR_ASSEMBLIFY_DATA_PATH` as shown below

```bash
# In run_assemblify.slurm
# 2 - Set directory containing working data sets (sequencing raw data) 
export ASSEMBLIFY_DATA_PATH=/YOUR_ASSEMBLIFY_DATA_PATH
if [ ! -e "$ASSEMBLIFY_DATA_PATH" ]; then
  echo "ERROR: $ASSEMBLIFY_DATA_PATH does not exist" >&2
  exit 1
fi
```

## Configuration files
All paths used along the pipeline are relative to parameters that can be modified in this file: ``conf/custom.config``. They should be modified if you want to change the species or if you want to change BUSCO database name. Some of them can also be defined in `run_assemblify.slurm`

## Changing container version

To change the version of a container or when adding a new software, you need to specify the image that will be automatically be pulled in the file `nextflow.config` like this;
```
withName PROCESS_NAME {
  container = "pulling link for container image"
}
```

## Redirection of errors

Errors and logging message are redirected. Logs can be redirected in `results/logs` and each process have their own logging directory. However, critical errors can be found in `assemblify.log` and in `.nextflow.log`, these files are directly in `nf_assemblify-base`. Finally, when encountering an error, you can also check in temporary directories created by Nextflow, temporary location are specified in `.nextflow.log` and many hidden files will help you for debugging.

# Pipeline overview

![Pipeline Overview](pipeline.png)

## File hierarchy

The results of the pipeline are stored in the directory as they come in `/results/assemblify` according to the following tree structure :
```
results
├── 00_pipeline_info
│   └── cmd
└── 01_reports
└── 02_results
    ├── logs
    │   ├── fastqc 
    │   ├── multiqc
    │   ├── nanoplot
    │   ├── hifiasm
    │   ├── busco
    │   ├── redmask
    │   ├── hisat2
    │   ├── samtools
    │   ├── braker3
    │   ├── multiqc_hisat2
    │   └── busco_after_braker3
    ├── 01a_fastqc
    ├── 01b_multiqc
    ├── 01c_nanoplot
    ├── 02a_hifiasm
    ├── 02b_busco
    ├── 03_redmask
    │   └── masked
    ├── 04a_hisat2
    ├── 04b_samtools
    ├── 04c_multiqc
    ├── 05a_braker3
    │   └── braker3
    └── 05b_busco
```
>In directory **00_pipeline_info**, you will find the commands used to check that the parameters are correctly configured.   
In directory **01_reports** you will find a spreadsheet with all kinds of information about the pipeline's progress. 

|task_id|hash|native_id | process | status |exit|queue|
|--------|--------|--------|--------|--------|--------|--------|
|1|9c/b364cc|37601568|nanoplot|COMPLETED|0|fast|


>And in directory **02_results**, all the sorted results files.
## Stage 1. Quality control

### 1.a FastQC
FastQC is a tool used in bioinformatics to assess the quality of genomic sequencing data. The version of the tool in use is 0.11.9.  
This is the Biocontainer used [fastqc:0.11.9--hdfd78af_1](https://quay.io/repository/biocontainers/fastqc?tab=tags&tag=0.11.9--hdfd78af_1) 
and its associated [GitHub](https://github.com/s-andrews/FastQC).

<table>
    <thead>
        <tr>
            <th>Input data</th>
            <th>Output data</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=2>Illumina sequencing files: <code>.fastq.gz</code></td>
            <td>Web report: <code>.html</code></td>
        </tr>
        <tr>
            <td>Archive: <code>.zip</code></td>
        </tr>
    </tbody>
</table>

#### Command line
```bash
fastqc ${FASTQ_FILE} -o .
```

### 1.b MultiQC
MultiQC is a tool to create a single report with interactive plots for multiple bioinformatics analyses across many samples. The version of the tool in use is 1.14.  
This is the Biocontainer used [multiqc:1.14--pyhdfd78af_0](https://quay.io/repository/biocontainers/multiqc?tab=tags&tag=1.14--pyhdfd78af_0) 
and its associated [GitHub](https://github.com/MultiQC/MultiQC).

| Input data|Output data|
|--------|--------|
|    Archive from FastQC: `.zip`    |    Web report: `.html`    |

#### Command line
```bash
multiqc --outdir . --filename $REPORT_NAME ${DIR}
```

### 1.c NanoPlot
Nanoplot is a bioinformatics tool specifically designed for the analysis and visualization of sequencing data generated by nanopore technologies, such as those provided by Oxford Nanopore Technologies (ONT).

This is the Biocontainer used [nanoplot:1.32.1--py_0](https://quay.io/repository/biocontainers/nanoplot?tab=tags&tag=1.32.1--py_0) 
and its associated [GitHub](https://github.com/wdecoster/NanoPlot).

<table>
    <thead>
        <tr>
            <th>Input data</th>
            <th>Output data</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=3>PacBio sequencing files: <code>.fastq.gz</code></td>
            <td>Web report: <code>.html</code></td>
        </tr>
        <tr>
            <td>Plot: <code>.png</code></td>
        </tr>
        <tr>
            <td>Statistics: <code>.txt</code></td>
        </tr>
    </tbody>
</table>

#### Command line
```bash
NanoPlot -t ${cpus} --plots kde dot hex --N50 --title \"PacBio Hifi reads for $(basename ${FASTQ_FILE%.hifi*})\" --fastq ${FASTQ_FILE} -o .
```

## Stage 2. Contiging
### 2.a Hifiasm
Hifiasm is a tool designed for the assembly of high-fidelity long reads, particularly those generated by high-accuracy sequencing technologies like HiFi reads from PacBio or other similar long-read platforms.

This is the Biocontainer used [hifiasm:0.18.9--h5b5514e_0](https://quay.io/repository/biocontainers/hifiasm?tab=tags&tag=0.18.9--h5b5514e_0) 
and its associated [GitHub](https://github.com/chhylp123/hifiasm).

<table>
    <thead>
        <tr>
            <th>Input data</th>
            <th>Output data</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=2>PacBio sequencing files: <code>.fastq.gz</code></td>
            <td>Assembly sequences: <code>.fasta</code> (ignore <code>hap1</code> and <code>hap2</code> )</td>
        </tr>
        <tr>
            <td>Assembly graph: <code>.gfa</code></td>
        </tr>
    </tbody>
</table>

#### Command line
```bash
hifiasm -k 51 -t ${NCPUS} ${HIFI}
```

### 2.b BUSCO
BUSCO, which stands for Benchmarking Universal Single-Copy Orthologs, is a bioinformatics tool used for the assessment of genome assembly and annotation completeness.

This is the Biocontainer used [busco:5.5.0--pyhdfd78af_0](https://quay.io/repository/biocontainers/busco?tab=tags&tag=5.5.0--pyhdfd78af_0)
and its associated [GitHub](https://github.com/Ensembl/ensembl-genes-nf).

<table>
    <thead>
        <tr>
            <th>Input data</th>
            <th>Output data</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=2>Assembly sequences from Hifiasm: <code>.fasta</code></td>
            <td>Subdirectories</td>
        </tr>
        <tr>
            <td>Statistics: <code>.tsv</code></td>
        </tr>
    </tbody>
</table>

#### Command line
```bash
busco -c $NCPUS -m genome --offline -i $ASSEMBLY_FILE -l ${BUSCO_DB_PATH}/${BUSCO_DB_NAME} -o $RES_FILE
```

## Stage 3. Reapat masking
### 3.a RED
RED, an intelligent, rapid, accurate tool for detecting repeats de-novo on the genomic scale.

This is the Biocontainer used [red:2018.09.10--h4ac6f70_2](https://quay.io/repository/biocontainers/red?tab=tags&tag=2018.09.10--h4ac6f70_2) 
and the [official tool repository](http://toolsmith.ens.utulsa.edu). Can also be found on [GitHub](https://github.com/BioinformaticsToolsmith/Red).

| Input data|Output data|
|--------|--------|
|    Assembly sequences from Hifiasm: `.fasta`    |    Softmasked assembly sequences: `.msk`     |

#### Command line
```bash
#Needs a directory as argument for input and output files
Red -gnm . -msk masked -cor $NCPUS
```


## Stage 4. Evidences
### 4.a HISAT2
HISAT2 is a fast and sensitive alignment program for mapping next-generation sequencing reads (whole-genome, transcriptome, and exome sequencing data) to a population of human genomes (as well as to a single reference genome).

This is the Biocontainer used [hisat2:2.2.1--h87f3376_5](https://quay.io/repository/biocontainers/hisat2?tab=tags&tag=2.2.1--h87f3376_5) 
and its associated [GitHub](https://github.com/DaehwanKimLab/hisat2).

| Input data|Output data|
|--------|--------|
|    Illumina sequencing files: `.fastq.gz`    |    Genome index: `.ht2`     |
|    Softmasked assembly sequences from RED: `.msk`    |    Mapping file: `.sam`     |

#### Command line
```bash
#Step 1 - Genome indexing
hisat2-build -p $NCPUS $MSK_FASTA ${MSK_FASTA%.*}

#Step 2 - Reads mapping and indexing
hisat2 -p $NCPUS --no-unal -q -x $IDX -1 ${R1} -2 ${R2} --max-intronlen $INTRON > $SAM_OUT"
```

### 4.b Samtools
Samtools for converting SAM to BAM and indexing.  
This is the Biocontainer used [samtools:1.19.2--h50ea8bc_0](https://quay.io/repository/biocontainers/samtools?tab=tags&tag=1.19.2--h50ea8bc_0) 
and its associated [GitHub](https://github.com/samtools/samtools).

<table>
    <thead>
        <tr>
            <th>Input data</th>
            <th>Output data</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=2>Mapping file: <code>.sam</code></td>
            <td>Binary mapping file: <code>.bam</code></td>
        </tr>
        <tr>
            <td>Mapping file index: <code>.bai</code></td>
        </tr>
    </tbody>
</table>

#### Command line
```bash
#Sorting
samtools sort -o $BAM_OUT -@ $NCPUS -m 2G -O bam -T tmp $SAM_IN

#Indexing
samtools index $BAM_OUT
```

### 4.c MultiQC
MultiQC is a tool to create a single report with interactive plots for multiple bioinformatics analyses across many samples. The version of the tool in use is 1.14.  
This is the Biocontainer used [multiqc:1.14--pyhdfd78af_0](https://quay.io/repository/biocontainers/multiqc?tab=tags&tag=1.14--pyhdfd78af_0) 
and its associated [GitHub](https://github.com/MultiQC/MultiQC).

| Input data|Output data|
|--------|--------|
|    Log files from Hisat2: `.log`    |    Web report: `.html`    |

#### Command line
```bash
multiqc --outdir . --filename $REPORT_NAME ${DIR}
```

## Stage 5. Prediction
### 5.a BRAKER3
BRAKER3 enables the usage of RNA-seq and protein data in a fully automated pipeline to train and predict highly reliable genes with GeneMark-ETP and AUGUSTUS. The result of the pipeline is the combined gene set of both gene prediction tools, which only contains genes with very high support from extrinsic evidence.  

This is the [teambraker/braker3:v3.0.7.1](https://hub.docker.com/layers/teambraker/braker3/v3.0.7.1/images/sha256-31fb998e8043dfa960eaab402c80d7bc9e488f225174c3512da6f1f75756f85a?context=explore) and its associated [GitHub](https://github.com/Gaius-Augustus/BRAKER).

<table>
    <thead>
        <tr>
            <th>Input data</th>
            <th>Output data</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=2>Softmasked assembly sequences from RED: <code>.msk</code></td>
            <td rowspan=3>Structural annotation : <code>.gff3</code></td>
        </tr>
        <tr></tr>
        <tr>
            <td rowspan=2>Mapped reads from Hisat2: <code>.bam</code></td>
        </tr>
        <tr>
            <td rowspan=3>Proteins sequences from annotation: <code>.faa</code></td>
        </tr>
        <tr>
            <td rowspan=2>Proteins from short evolutionary distance: <code>.faa</code></td>
        </tr>
        <tr></tr>
    </tbody>
</table>

#### Command line
```bash
braker.pl --species=${SPECIES} --genome=${MSK_FASTA} --bam=${RNASEQ_BAM} --prot_seq=${PROT} --workingdir=braker3 --gff3 --threads ${NCPUS}
```


### 5.b BUSCO (after BRAKER3)
BUSCO, which stands for Benchmarking Universal Single-Copy Orthologs, is a bioinformatics tool used for the assessment of genome assembly and annotation completeness.

This is the Biocontainer used [busco:5.5.0--pyhdfd78af_0](https://quay.io/repository/biocontainers/busco?tab=tags&tag=5.5.0--pyhdfd78af_0) 
and its associated [GitHub](https://github.com/Ensembl/ensembl-genes-nf).

<table>
    <thead>
        <tr>
            <th>Input data</th>
            <th>Output data</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=2>Assembly sequences from Hifiasm: <code>.fasta</code></td>
            <td>Subdirectories</td>
        </tr>
        <tr>
            <td>Statistics: <code>.tsv</code></td>
        </tr>
    </tbody>
</table>

#### Command line
```bash
busco -c $NCPUS -m proteins --offline -i $PROT_FILE -l ${BUSCO_DB_PATH}/${BUSCO_DB_NAME} -o $RES_FILE
```

# Version control (Git branches)

![Version control](version.png)

# License
[GNU Affero General Public License v3.0](https://gitlab.com/VictorBerthod/nf_assemblify-base/-/blob/master/LICENSE)  
>You can learn more about this license [here](https://www.gnu.org/licenses/agpl-3.0.txt).

# Authors
Victor BERTHOD, M2 Bio-informatique  
Alix REGNIER, M2 Bio-informatique

**For further informations please contact us :  
[Victor Berthod](mailto:victor.berthod@etudiant.univ-rennes.fr)
[Alix Regnier](mailto:alix.regnier@etudiant.univ-rennes.fr)**